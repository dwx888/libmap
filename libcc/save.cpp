#include "save.h"
#include <fstream>
#include "stdlib.h"

#include "../libpng/png.h"
#pragma comment(lib, "library/libpng")
#pragma comment(lib, "library/libzlib")
static bool hasAlpha() { return true; }
bool save2png(const char* filename, unsigned char* _data, int _width, int _height)
{
	bool isToRGB = false;

	// #if CC_USE_WIC
	// 	return encodeWithWIC(filePath, isToRGB, GUID_ContainerFormatPng);
	// #elif CC_USE_PNG
	bool ret = false;
	do
	{
		FILE *fp = nullptr;
		png_structp png_ptr;
		png_infop info_ptr;
		png_colorp palette;
		png_bytep *row_pointers;

		// 		fp = fopen(FileUtils::getInstance()->getSuitableFOpen(filePath).c_str(), "wb");
		//		CC_BREAK_IF(nullptr == fp);
		fp = fopen(filename, "wb");
		if (fp == nullptr)
		{
			break;
		}

		png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);

		if (nullptr == png_ptr)
		{
			fclose(fp);
			break;
		}

		info_ptr = png_create_info_struct(png_ptr);
		if (nullptr == info_ptr)
		{
			fclose(fp);
			png_destroy_write_struct(&png_ptr, nullptr);
			break;
		}


#if (CC_TARGET_PLATFORM != CC_PLATFORM_BADA && CC_TARGET_PLATFORM != CC_PLATFORM_NACL && CC_TARGET_PLATFORM != CC_PLATFORM_TIZEN)
		if (setjmp(png_jmpbuf(png_ptr)))
		{
			fclose(fp);
			png_destroy_write_struct(&png_ptr, &info_ptr);
			break;
		}
#endif
		png_init_io(png_ptr, fp);

		if (!isToRGB && hasAlpha())
		{
			png_set_IHDR(png_ptr, info_ptr, _width, _height, 8, PNG_COLOR_TYPE_RGB_ALPHA,
				PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
		}
		else
		{
			png_set_IHDR(png_ptr, info_ptr, _width, _height, 8, PNG_COLOR_TYPE_RGB,
				PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
		}

		palette = (png_colorp)png_malloc(png_ptr, PNG_MAX_PALETTE_LENGTH * sizeof(png_color));
		png_set_PLTE(png_ptr, info_ptr, palette, PNG_MAX_PALETTE_LENGTH);

		png_write_info(png_ptr, info_ptr);

		png_set_packing(png_ptr);

		row_pointers = (png_bytep *)malloc(_height * sizeof(png_bytep));
		if (row_pointers == nullptr)
		{
			fclose(fp);
			png_destroy_write_struct(&png_ptr, &info_ptr);
			break;
		}

		if (!hasAlpha())
		{
			for (int i = 0; i < (int)_height; i++)
			{
				row_pointers[i] = (png_bytep)_data + i * _width * 3;
			}

			png_write_image(png_ptr, row_pointers);

			free(row_pointers);
			row_pointers = nullptr;
		}
		else
		{
			if (isToRGB)
			{
				unsigned char *tempData = static_cast<unsigned char*>(malloc(_width * _height * 3 * sizeof(unsigned char)));
				if (nullptr == tempData)
				{
					fclose(fp);
					png_destroy_write_struct(&png_ptr, &info_ptr);

					free(row_pointers);
					row_pointers = nullptr;
					break;
				}

				for (int i = 0; i < _height; ++i)
				{
					for (int j = 0; j < _width; ++j)
					{
						tempData[(i * _width + j) * 3] = _data[(i * _width + j) * 4];
						tempData[(i * _width + j) * 3 + 1] = _data[(i * _width + j) * 4 + 1];
						tempData[(i * _width + j) * 3 + 2] = _data[(i * _width + j) * 4 + 2];
					}
				}

				for (int i = 0; i < (int)_height; i++)
				{
					row_pointers[i] = (png_bytep)tempData + i * _width * 3;
				}

				png_write_image(png_ptr, row_pointers);

				free(row_pointers);
				row_pointers = nullptr;

				if (tempData != nullptr)
				{
					free(tempData);
				}
			}
			else
			{
				for (int i = 0; i < (int)_height; i++)
				{
					row_pointers[i] = (png_bytep)_data + i * _width * 4;
				}

				png_write_image(png_ptr, row_pointers);

				free(row_pointers);
				row_pointers = nullptr;
			}
		}

		png_write_end(png_ptr, info_ptr);

		png_free(png_ptr, palette);
		palette = nullptr;

		png_destroy_write_struct(&png_ptr, &info_ptr);

		fclose(fp);

		ret = true;
	} while (0);
	return ret;
	// #else
	// 	CCLOG("png is not enabled, please enable it in ccConfig.h");
	// 	return false;
	// #endif // CC_USE_PNG
}


using BYTE = uchar;
using WORD = ushort;
struct TGAFILEHEADER {
	BYTE bIDLength;						//附加信息长度
	BYTE bPalType;						//调色板信息(不支持)
	BYTE bImageType;					//图象类型(只支持 3,10)
	WORD wPalIndex;						//调色板第一个索引值
	WORD wPalLength;					//调色板索引数
	BYTE bPalBits;						//一个调色板单位位数(15,16,24,32)
	WORD wLeft;							//图象左端坐标(基本不用)
	WORD wBottom;						//图象底端坐标(基本不用)
	WORD wWidth;						//图象宽度
	WORD wHeight;						//图象高度
	BYTE bBits;							//一个象素位数
	BYTE bAlphaBits : 4;				//Alpha位数
// 	BYTE bReserved : 1;					//保留,必须为0
// 	BYTE bTopDown : 1;					//为1表示从上到下(只支持从下到上)
// 	BYTE bInterleaving : 2;				//隔行(一般为0)
};

void save2tga(const std::string& filename, uint* data, int width, int height)
{
	TGAFILEHEADER head;
	memset(&head, 0, sizeof(TGAFILEHEADER));
	head.bImageType = 2;	// 不压缩
	head.bBits = 32;		// 32bit
	head.wWidth = width;
	head.wHeight = height;
	head.bAlphaBits = 8;

	std::ofstream file(filename, std::ios::binary);
	// 	if (file.is_open())
	// 	{
	// 		file.write((char*)&head, sizeof(TGAFILEHEADER));
	// 		file.write((char*)data, width * height * 4);
	// 		file.close();
	// 	}
	// 	return;

	uchar type_header[12] = { 0,0,2,0,0,0,0,0,0,0,0,0 };

	uchar header[6];
	header[0] = width % 256;
	header[1] = width / 256;
	header[2] = height % 256;
	header[3] = height / 256;
	header[4] = 32;
	header[5] = 8;

	if (file.is_open())
	{
		file.write((char*)type_header, 12);
		file.write((char*)header, 6);

		uchar* newchar = new uchar[width * 4];
		uchar* c;
		for (int k = height - 1, i; k >= 0; --k)
		{
			memcpy(newchar, data + k * width, width * 4);
			c = newchar;
			for (i = 0; i < width; ++i, c += 4)
			{
				std::swap(c[0], c[2]);
			}
			file.write((char*)newchar, width * 4);
		}
		delete[] newchar;
		file.close();
	}
}
